package com.vladstarikov.noadstube.data.db.converters

import androidx.room.TypeConverter
import java.time.Instant

class InstantTypeConverter {

    @TypeConverter
    fun from(timestamp: Long): Instant = Instant.ofEpochMilli(timestamp)

    @TypeConverter
    fun to(instant: Instant): Long = instant.toEpochMilli()
}