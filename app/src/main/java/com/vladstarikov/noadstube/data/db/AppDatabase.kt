package com.vladstarikov.noadstube.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.vladstarikov.noadstube.data.db.dao.LogRecordDao
import com.vladstarikov.noadstube.data.db.entities.LogRecord

@Database(
    entities = [LogRecord::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun logRecordDao(): LogRecordDao

    companion object {

        fun build(context: Context) = Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            "database"
        ).build()
    }
}