package com.vladstarikov.noadstube.data.entities

import kotlin.time.DurationUnit

/**
 * @param count allowed number of shorts (0 - block all shorts)
 * @param durationUnit duration unit of [count]
 */
data class AllowedShortsCount(
    val count: Int = 0,
    val durationUnit: DurationUnit = DurationUnit.DAYS,
) {
    companion object {
        fun deserialize(string: String?): AllowedShortsCount? {
            if (string == null) return null
            val split = string.split("/")
            val count = split[0].toInt()
            val durationUnit = DurationUnit.entries.first { it.name == split[1] }
            return AllowedShortsCount(count, durationUnit)
        }
    }

    fun serialize(): String = "$count/$durationUnit"
}