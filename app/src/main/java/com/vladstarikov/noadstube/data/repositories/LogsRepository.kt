package com.vladstarikov.noadstube.data.repositories

import com.vladstarikov.noadstube.data.db.dao.LogRecordDao
import com.vladstarikov.noadstube.data.db.entities.LogRecord
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.Channel.Factory.UNLIMITED
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.receiveAsFlow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LogsRepository @Inject constructor(private val logRecordDao: LogRecordDao) {

    private val coroutineScope = CoroutineScope(SupervisorJob() + Dispatchers.IO)

    private val logsChannel: Channel<LogRecord> = Channel(
        capacity = UNLIMITED,
        onBufferOverflow = BufferOverflow.DROP_OLDEST
    )

    init {
        logsChannel.receiveAsFlow().onEach { logRecordDao.insert(it) }.launchIn(coroutineScope)
    }

    fun log(record: LogRecord) {
        logsChannel.trySend(record)
    }

    fun countWithTypeAsFlow(
        type: LogRecord.Type
    ): Flow<Int> = logRecordDao.countWithTypeAsFlow(type)
}