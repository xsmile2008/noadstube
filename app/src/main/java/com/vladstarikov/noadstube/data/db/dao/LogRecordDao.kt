package com.vladstarikov.noadstube.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.vladstarikov.noadstube.data.db.entities.LogRecord
import kotlinx.coroutines.flow.Flow

@Dao
interface LogRecordDao {

    @Query("SELECT * FROM LogRecord")
    fun selectAll(): List<LogRecord>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(logRecord: LogRecord)

    @Query("SELECT COUNT (*) FROM LogRecord WHERE type == :type")
    fun countWithTypeAsFlow(type: LogRecord.Type): Flow<Int>
}