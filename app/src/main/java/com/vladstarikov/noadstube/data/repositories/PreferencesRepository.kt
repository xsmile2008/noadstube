package com.vladstarikov.noadstube.data.repositories

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import com.vladstarikov.noadstube.data.entities.AllowedShortsCount
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PreferencesRepository @Inject constructor(private val dataStore: DataStore<Preferences>) {

    private val coroutineScope: CoroutineScope = CoroutineScope(SupervisorJob() + Dispatchers.IO)

    object Key {
        val BLOCK_AD = booleanPreferencesKey("BLOCK_AD")
        val SKIP_AD = booleanPreferencesKey("SKIP_AD")
        val MUTE_AD = booleanPreferencesKey("MUTE_AD")
        val SKIP_SURVEYS = booleanPreferencesKey("SKIP_SURVEYS")
        val ALLOWED_SHORTS_COUNT = stringPreferencesKey("ALLOWED_SHORTS_COUNT")
        val SHOW_REPORTS = booleanPreferencesKey("SHOW_REPORTS")
    }

    // region blockAd

    val blockAd: StateFlow<Boolean> by lazy {
        runBlocking {
            dataStore.data.map { it[Key.BLOCK_AD] ?: true }.stateIn(coroutineScope)
        }
    }

    suspend fun updateBlockAd(value: Boolean) {
        dataStore.edit {
            it[Key.BLOCK_AD] = value
        }
    }
    // endregion

    // region skipAd

    val skipAd: StateFlow<Boolean> by lazy {
        runBlocking {
            dataStore.data.map { it[Key.SKIP_AD] ?: true }.stateIn(coroutineScope)
        }
    }

    suspend fun updateSkipAd(value: Boolean) {
        dataStore.edit {
            it[Key.SKIP_AD] = value
        }
    }
    // endregion

    // region muteAd

    val muteAd: StateFlow<Boolean> by lazy {
        runBlocking {
            dataStore.data.map { it[Key.MUTE_AD] ?: true }.stateIn(coroutineScope)
        }
    }

    suspend fun updateMuteAd(value: Boolean) {
        dataStore.edit {
            it[Key.MUTE_AD] = value
        }
    }
    // endregion

    // region skipSurveys

    val skipSurveys: StateFlow<Boolean> by lazy {
        runBlocking {
            dataStore.data.map { it[Key.SKIP_SURVEYS] ?: true }.stateIn(coroutineScope)
        }
    }

    suspend fun updateSkipSurveys(value: Boolean) {
        dataStore.edit {
            it[Key.SKIP_SURVEYS] = value
        }
    }
    // endregion

    // region allowedShortsCount

    val allowedShortsCount: StateFlow<AllowedShortsCount?> by lazy {
        runBlocking {
            dataStore.data.map {
                AllowedShortsCount.deserialize(
                    it[Key.ALLOWED_SHORTS_COUNT]
                )
            }.stateIn(coroutineScope)
        }
    }

    suspend fun updateAllowedShortsCount(value: AllowedShortsCount?) {
        dataStore.edit {
            if (value == null) {
                it.remove(Key.ALLOWED_SHORTS_COUNT)
            } else {
                it[Key.ALLOWED_SHORTS_COUNT] = value.serialize()
            }
        }
    }
    // endregion

    // region showReports

    val showReports: StateFlow<Boolean> by lazy {
        runBlocking {
            dataStore.data.map { it[Key.SHOW_REPORTS] ?: true }.stateIn(coroutineScope)
        }
    }

    suspend fun updateShowReports(value: Boolean) {
        dataStore.edit {
            it[Key.SHOW_REPORTS] = value
        }
    }
    // endregion

    init {
        // Workaround to init all values on background thread
        coroutineScope.launch {
            blockAd // Init by lazy
            skipAd // Init by lazy
            muteAd // Init by lazy
            skipSurveys // Init by lazy
            showReports // Init by lazy
        }
    }
}