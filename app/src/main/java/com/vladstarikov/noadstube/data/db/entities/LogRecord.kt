package com.vladstarikov.noadstube.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.vladstarikov.noadstube.data.db.converters.InstantTypeConverter
import java.time.Instant

@Entity
@TypeConverters(
    InstantTypeConverter::class,
    LogRecord.Type::class
)
data class LogRecord(
    @PrimaryKey val date: Instant = Instant.now(),
    val type: Type
) {

    enum class Type {
        AD_BLOCKED,
        AD_SKIPPED,
        SURVEY_SKIPPED,
        SHORT_BLOCKED;

        companion object {

            @JvmStatic
            @TypeConverter
            fun to(value: Type?): String? = value?.name

            @JvmStatic
            @TypeConverter
            fun from(value: String?): Type? = value?.let { valueOf(it) }
        }
    }
}