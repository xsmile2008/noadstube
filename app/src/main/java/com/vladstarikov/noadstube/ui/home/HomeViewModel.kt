package com.vladstarikov.noadstube.ui.home

import androidx.lifecycle.ViewModel
import com.vladstarikov.noadstube.data.db.entities.LogRecord
import com.vladstarikov.noadstube.data.repositories.LogsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    logsRepository: LogsRepository
) : ViewModel() {

    sealed class ViewState {
        data object Loading : ViewState()
        data class Loaded(
            val blockedAdsCount: Int,
            val skippedAdsCount: Int,
            val skippedSurveysCount: Int,
            val blockedShortsCount: Int,
        ) : ViewState()
    }


    private val _viewState: Flow<ViewState> = combine(
        logsRepository.countWithTypeAsFlow(type = LogRecord.Type.AD_BLOCKED),
        logsRepository.countWithTypeAsFlow(type = LogRecord.Type.AD_SKIPPED),
        logsRepository.countWithTypeAsFlow(type = LogRecord.Type.SURVEY_SKIPPED),
        logsRepository.countWithTypeAsFlow(type = LogRecord.Type.SHORT_BLOCKED),
    ) { (blockedAdsCount, skippedAdsCount, skippedSurveysCount, blockedShortsCount) ->
        ViewState.Loaded(
            blockedAdsCount,
            skippedAdsCount,
            skippedSurveysCount,
            blockedShortsCount,
        )
    }
    val viewState: Flow<ViewState> = _viewState
}