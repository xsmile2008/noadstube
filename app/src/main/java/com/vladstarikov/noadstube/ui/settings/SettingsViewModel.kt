package com.vladstarikov.noadstube.ui.settings

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.vladstarikov.noadstube.data.entities.AllowedShortsCount
import com.vladstarikov.noadstube.data.repositories.PreferencesRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SettingsViewModel @Inject constructor(
    private val preferencesRepository: PreferencesRepository
) : ViewModel(), LifecycleObserver {

    sealed class ViewState {
        data object Loading : ViewState()
        data class Loaded(
            val blockAd: Boolean,
            val skipAd: Boolean,
            val muteAd: Boolean,
            val skipSurveys: Boolean,
            val allowedShortsCount: AllowedShortsCount?,
            val showReports: Boolean
        ) : ViewState()
    }

    private val _viewState: Flow<ViewState> = combine(
        preferencesRepository.blockAd,
        preferencesRepository.skipAd,
        preferencesRepository.muteAd,
        preferencesRepository.skipSurveys,
        preferencesRepository.allowedShortsCount,
        preferencesRepository.showReports
    ) { values ->
        ViewState.Loaded(
            values[0] as Boolean,
            values[1] as Boolean,
            values[2] as Boolean,
            values[3] as Boolean,
            values[4] as AllowedShortsCount?,
            values[5] as Boolean
        )
    }
    val viewState: Flow<ViewState> = _viewState

    fun onSkipAdChange(value: Boolean) {
        viewModelScope.launch {
            preferencesRepository.updateSkipAd(value)
        }
    }

    fun onBlockAdChange(value: Boolean) {
        viewModelScope.launch {
            preferencesRepository.updateBlockAd(value)
        }
    }

    fun onMuteAdChange(value: Boolean) {
        viewModelScope.launch {
            preferencesRepository.updateMuteAd(value)
        }
    }

    fun onSkipSurveysChange(value: Boolean) {
        viewModelScope.launch {
            preferencesRepository.updateSkipSurveys(value)
        }
    }

    fun onAllowedShortsCountChange(value: AllowedShortsCount?) {
        viewModelScope.launch {
            preferencesRepository.updateAllowedShortsCount(value)
        }
    }

    fun onShowReportsChange(value: Boolean) {
        viewModelScope.launch {
            preferencesRepository.updateShowReports(value)
        }
    }
}