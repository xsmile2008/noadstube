package com.vladstarikov.noadstube.ui.root

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel
import com.vladstarikov.noadstube.accessibility.AccessibilityManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.receiveAsFlow
import javax.inject.Inject

@HiltViewModel
class RootViewModel @Inject constructor(
    private val accessibilityManager: AccessibilityManager
) : ViewModel(), LifecycleEventObserver {

    sealed class ViewAction {
        object OpenPermissionsScreen : ViewAction()
    }

    private val _viewAction = Channel<ViewAction>(capacity = Channel.UNLIMITED)
    val viewAction: Flow<ViewAction> = _viewAction.receiveAsFlow()

    override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
        when(event.targetState) {
            Lifecycle.State.STARTED -> {
                if (!accessibilityManager.isAccessibilitySettingOn) {
                    _viewAction.trySend(ViewAction.OpenPermissionsScreen)
                }
            }
            else -> {
                // Nothing
            }
        }
    }
}