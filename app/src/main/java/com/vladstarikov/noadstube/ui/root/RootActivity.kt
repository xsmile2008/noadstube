package com.vladstarikov.noadstube.ui.root

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.runtime.rememberCoroutineScope
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.vladstarikov.noadstube.ui.main.MainScreen
import com.vladstarikov.noadstube.ui.permissions.PermissionsScreen
import com.vladstarikov.noadstube.ui.values.AppTheme
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class RootActivity : AppCompatActivity() {

    private val viewModel: RootViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        lifecycle.addObserver(viewModel)

        setContent {
            AppTheme {
                val navController = rememberNavController()
                NavHost(
                    navController = navController,
                    startDestination = "main"
                ) {
                    composable("main") {
                        MainScreen()
                    }
                    composable("permissions") {
                        PermissionsScreen(navController, this@RootActivity)
                    }
                }

                viewModel.viewAction.onEach {
                    when (it) {
                        RootViewModel.ViewAction.OpenPermissionsScreen -> {
                            navController.navigate("permissions") {
                                launchSingleTop = true
                                popUpTo("main") {
                                    inclusive = true
                                }
                            }
                        }
                    }
                }.launchIn(rememberCoroutineScope())
            }
        }
    }
}