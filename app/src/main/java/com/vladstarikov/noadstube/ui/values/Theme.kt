package com.vladstarikov.noadstube.ui.values

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable

private val DarkColorPalette = darkColors(
    primary = red,
    primaryVariant = freeSpeechRed,
    secondary = outrageousOrange,
    background = nero,
    surface = matterhorn,
    onPrimary = white,
    onBackground = whisper,
    onSurface = whisper
)

private val LightColorPalette = lightColors(
    primary = red,
    primaryVariant = freeSpeechRed,
    secondary = outrageousOrange,
    background = white,
    surface = whisper,
    onPrimary = white,
    onBackground = nero,
    onSurface = nero
)

@Composable
fun AppTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    MaterialTheme(
        colors = if (darkTheme) DarkColorPalette else LightColorPalette,
        typography = typography,
        shapes = shapes,
        content = content
    )
}
