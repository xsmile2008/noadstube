package com.vladstarikov.noadstube.ui.values

import androidx.compose.ui.graphics.Color

val red = Color.Red
val white = Color.White
val black = Color.Black
val outrageousOrange = Color(0xFFFF5A36)
val freeSpeechRed = Color(0xFFC20000)
val matterhorn = Color(0xFF505050)
val nero = Color(0xFF282828)
val whisper = Color(0xFFEEEEEE)
val silver = Color(0xFFBCBCBC)
val sunglow = Color(0xFFFFD636)
val neonCarrot = Color(0xFFFF8A36)

