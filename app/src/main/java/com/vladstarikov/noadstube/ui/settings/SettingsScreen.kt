@file:OptIn(ExperimentalMaterialApi::class, ExperimentalMaterialApi::class)

package com.vladstarikov.noadstube.ui.settings

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Card
import androidx.compose.material.Divider
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ExposedDropdownMenuBox
import androidx.compose.material.ExposedDropdownMenuDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Switch
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.vladstarikov.noadstube.R
import com.vladstarikov.noadstube.data.entities.AllowedShortsCount
import com.vladstarikov.noadstube.ui.common.BadgeText
import com.vladstarikov.noadstube.ui.settings.SettingsViewModel.ViewState
import org.jetbrains.annotations.TestOnly
import kotlin.time.DurationUnit

@Composable
fun SettingsScreen() {
    val viewModel: SettingsViewModel = hiltViewModel()

    Content(
        viewState = viewModel.viewState.collectAsState(initial = ViewState.Loading).value,
        onSkipAdChange = viewModel::onSkipAdChange,
        onBlockAdChange = viewModel::onBlockAdChange,
        onMuteAdChange = viewModel::onMuteAdChange,
        onSkipSurveysChange = viewModel::onSkipSurveysChange,
        onAllowedShortsCountChange = viewModel::onAllowedShortsCountChange,
        onShowReportsChange = viewModel::onShowReportsChange
    )
}

@Preview
@Composable
private fun Content(
    viewState: ViewState = ViewState.Loading,
    onSkipAdChange: (Boolean) -> Unit = {},
    onBlockAdChange: (Boolean) -> Unit = {},
    onMuteAdChange: (Boolean) -> Unit = {},
    onSkipSurveysChange: (Boolean) -> Unit = {},
    onAllowedShortsCountChange: (AllowedShortsCount?) -> Unit = {},
    onShowReportsChange: (Boolean) -> Unit = {},
) {
    when (viewState) {
        ViewState.Loading -> {
            // Nothing
        }

        is ViewState.Loaded -> {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .verticalScroll(rememberScrollState())
                    .padding(16.dp, 24.dp, 16.dp, 80.dp),
                verticalArrangement = Arrangement.spacedBy(16.dp)
            ) {
                Preference(
                    title = stringResource(id = R.string.preference_skip_ad_title),
                    description = stringResource(R.string.preference_skip_ad_description),
                    checked = viewState.skipAd,
                    onCheckedChange = onSkipAdChange
                )
//                Preference(
//                    title = stringResource(id = R.string.preference_block_ad_title),
//                    description = stringResource(id = R.string.preference_block_ad_description),
//                    checked = viewState.blockAd,
//                    onCheckedChange = onBlockAdChange,
//                    isExperimental = true
//                )
                Preference(
                    title = stringResource(id = R.string.preference_mute_ad_title),
                    description = stringResource(id = R.string.preference_mute_ad_description),
                    checked = viewState.muteAd,
                    onCheckedChange = onMuteAdChange,
                    isComingSoon = true
                )
                Preference(
                    title = stringResource(id = R.string.preference_skip_surveys_title),
                    description = stringResource(id = R.string.preference_skip_surveys_description),
                    checked = viewState.skipSurveys,
                    onCheckedChange = onSkipSurveysChange,
                    isExperimental = true
                )
                Preference(
                    title = stringResource(id = R.string.preference_block_shorts),
                    description = stringResource(id = R.string.preference_block_shorts_description),
                    checked = viewState.allowedShortsCount != null,
                    onCheckedChange = { checked ->
                        onAllowedShortsCountChange(if (checked) AllowedShortsCount() else null)
                    },
                    checkedContent = {
                        BlockShortsCheckedContent(
                            allowedShortsCount = viewState.allowedShortsCount!!,
                            onAllowedShortsCountChange = onAllowedShortsCountChange
                        )
                    }
                )
                Preference(
                    title = stringResource(id = R.string.preference_show_reports_title),
                    description = stringResource(id = R.string.preference_show_reports_description),
                    checked = viewState.showReports,
                    onCheckedChange = onShowReportsChange
                )
            }
        }
    }
}

@Composable
private fun BlockShortsCheckedContent(
    allowedShortsCount: AllowedShortsCount,
    onAllowedShortsCountChange: (AllowedShortsCount?) -> Unit = {},
) {
    Row(verticalAlignment = Alignment.CenterVertically) {
        var expandedCount by remember { mutableStateOf(false) }
        ExposedDropdownMenuBox(
            modifier = Modifier.weight(1f),
            expanded = expandedCount,
            onExpandedChange = { expandedCount = !expandedCount },
            content = {
                TextField(
                    value = allowedShortsCount.count.toString(),
                    onValueChange = {},
                    readOnly = true,
                    trailingIcon = {
                        ExposedDropdownMenuDefaults.TrailingIcon(
                            expanded = expandedCount
                        )
                    },
                )

                ExposedDropdownMenu(
                    expanded = expandedCount,
                    onDismissRequest = { expandedCount = false }) {
                    (0..100).forEach {
                        DropdownMenuItem(
                            content = { Text(text = it.toString()) },
                            onClick = {
                                expandedCount = false
                                onAllowedShortsCountChange(
                                    allowedShortsCount.copy(count = it)
                                )
                            }
                        )
                    }
                }
            }
        )

        Text(
            modifier = Modifier.padding(horizontal = 16.dp),
            text = stringResource(id = R.string.preference_block_shorts_per),
        )

        var expandedDurationUnit by remember { mutableStateOf(false) }
        ExposedDropdownMenuBox(
            modifier = Modifier.weight(1f),
            expanded = expandedDurationUnit,
            onExpandedChange = { expandedDurationUnit = !expandedDurationUnit },
            content = {
                TextField(
                    value = allowedShortsCount.durationUnit.name.lowercase()
                        .capitalize()
                        .dropLast(1),
                    onValueChange = {},
                    readOnly = true,
                    trailingIcon = {
                        ExposedDropdownMenuDefaults.TrailingIcon(
                            expanded = expandedDurationUnit
                        )
                    },
                )

                ExposedDropdownMenu(
                    expanded = expandedDurationUnit,
                    onDismissRequest = { expandedDurationUnit = false }) {
                    listOf(
                        DurationUnit.HOURS,
                        DurationUnit.DAYS,
                    ).forEach {
                        DropdownMenuItem(
                            content = {
                                Text(
                                    text = it.name.lowercase().capitalize()
                                        .dropLast(1)
                                )
                            },
                            onClick = {
                                expandedDurationUnit = false
                                onAllowedShortsCountChange(
                                    allowedShortsCount.copy(durationUnit = it)
                                )
                            }
                        )
                    }
                }
            }
        )
    }
}

@Preview
@Composable
@TestOnly
private fun BlockShortsCheckedContentPreview() {
    Card {
        BlockShortsCheckedContent(
            allowedShortsCount = AllowedShortsCount(),
            onAllowedShortsCountChange = {}
        )
    }
}

@Composable
private fun Preference(
    title: String,
    description: String,
    checked: Boolean,
    onCheckedChange: (Boolean) -> Unit,
    isExperimental: Boolean = false,
    isComingSoon: Boolean = false,
    checkedContent: (@Composable () -> Unit)? = null
) {
    Card(modifier = Modifier.fillMaxWidth()) {
        Box(modifier = Modifier.fillMaxWidth()) {
            when {
                isComingSoon -> BadgeText(
                    modifier = Modifier
                        .align(Alignment.TopEnd)
                        .padding(8.dp),
                    text = stringResource(R.string.coming_soon),
                    color = MaterialTheme.colors.secondary
                )

                isExperimental -> BadgeText(
                    modifier = Modifier
                        .align(Alignment.TopEnd)
                        .padding(8.dp),
                    text = stringResource(R.string.experimental),
                    color = MaterialTheme.colors.primary
                )
            }

            Column {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 16.dp, vertical = 16.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Column(
                        modifier = Modifier
                            .weight(1f)
                            .padding(end = 16.dp)
                    ) {
                        Text(
                            text = title,
                            style = MaterialTheme.typography.h5,
//                          color = MaterialTheme.colors.onPrimary//TODO: wrong way to do it?
                        )
                        Text(text = description, Modifier.padding(top = 4.dp))
                    }
                    Switch(
                        enabled = !isComingSoon,
                        checked = !isComingSoon && checked,
                        onCheckedChange = onCheckedChange
                    )
                }

                if (checked && checkedContent != null) {
                    Divider(modifier = Modifier.padding(horizontal = 16.dp))

                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(horizontal = 16.dp, vertical = 16.dp),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        checkedContent()
                    }
                }
            }
        }
    }
}

@Preview
@Composable
@TestOnly
private fun PreferencePreview() {
    Preference(
        title = "Lorem ipsum",
        description = "Lorem ipsum sit dolor",
        checked = true,
        onCheckedChange = {}
    )
}