package com.vladstarikov.noadstube.ui.main

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.vladstarikov.noadstube.R
import com.vladstarikov.noadstube.ui.home.HomeScreen
import com.vladstarikov.noadstube.ui.settings.SettingsScreen

enum class Tab(
    val route: String,
    @DrawableRes
    val iconRes: Int,
    @StringRes
    val titleRes: Int
) {
    HOME(
        "home",
        R.drawable.ic_tab_home,
        R.string.tab_home
    ),
    SETTINGS(
        "settings",
        R.drawable.ic_tab_settings,
        R.string.tab_settings
    )
}

@Composable
fun MainScreen() {
    Content()
}

@Preview
@Composable
private fun Content() {
    val navController = rememberNavController()
    Scaffold(
        bottomBar = {
            BottomNavigation {
                val currentRoute =
                    navController.currentBackStackEntryAsState().value?.destination?.route
                Tab.values().forEach { tab ->
                    BottomNavigationItem(
                        icon = {
                            Icon(
                                painter = painterResource(id = tab.iconRes),
                                contentDescription = stringResource(id = tab.titleRes)
                            )
                        },
                        label = { Text(text = stringResource(id = tab.titleRes)) },
                        selected = currentRoute == tab.route,
                        onClick = {
                            if (currentRoute != tab.route) {
                                navController.navigate(tab.route) {
                                    currentRoute?.let {
                                        popUpTo(it) {
                                            inclusive = true
                                        }
                                    }
                                    launchSingleTop = true
                                }
                            }
                        }
                    )
                }
            }
        }
    ) {
        NavHost(
            navController = navController,
            startDestination = Tab.HOME.route
        ) {
            composable(Tab.HOME.route) {
                HomeScreen()
            }
            composable(Tab.SETTINGS.route) {
                SettingsScreen()
            }
        }
    }
}