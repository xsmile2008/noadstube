package com.vladstarikov.noadstube.ui.permissions

import android.content.Intent
import android.provider.Settings
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.vladstarikov.noadstube.R
import com.vladstarikov.noadstube.ui.permissions.PermissionsViewModel.ViewAction
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@Composable
fun PermissionsScreen(
    navController: NavController = rememberNavController(),
    lifecycleOwner: LifecycleOwner
) {
    val viewModel: PermissionsViewModel = hiltViewModel()
    val coroutineScope = rememberCoroutineScope()
    val context = LocalContext.current
    lifecycleOwner.lifecycle.addObserver(viewModel)
    viewModel.viewAction.onEach {
        when (it) {
            ViewAction.OpenHomeScreen -> {
                navController.navigate("main")
            }
            ViewAction.OpenAccessibilitySettings -> {
                context.startActivity(
                    Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS)
                        .setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
                )
            }
        }
    }.launchIn(coroutineScope)

    Content(
        onGrandPermissionClicked = { viewModel.onGrantPermissionClicked() }
    )
}

@Preview
@Composable
private fun Content(
    onGrandPermissionClicked: () -> Unit = {}
) {
    Scaffold {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(start = 16.dp, end = 16.dp, bottom = 24.dp)
        ) {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .weight(1f)
                    .verticalScroll(rememberScrollState())
                    .padding(top = 24.dp, bottom = 16.dp)
            ) {
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    text = stringResource(id = R.string.welcome_to_app),
                    textAlign = TextAlign.Center,
                    style = MaterialTheme.typography.h3,
//                    color = MaterialTheme.colors.onPrimary//TODO: wrong way to do it?
                )
                Image(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(200.dp)
                        .padding(vertical = 16.dp),
                    alignment = Alignment.Center,
                    painter = painterResource(id = R.drawable.img_cat_1),
                    contentDescription = null
                )
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    text = stringResource(id = R.string.accessibility_permission_description)
                )
            }
            Button(
                modifier = Modifier.fillMaxWidth(),
                onClick = { onGrandPermissionClicked() }
            ) {
                Text(text = stringResource(R.string.action_grand_permission).uppercase())
            }
        }
    }
}