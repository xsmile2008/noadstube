package com.vladstarikov.noadstube.ui.home

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.vladstarikov.noadstube.R
import com.vladstarikov.noadstube.ui.home.HomeViewModel.ViewState

@Composable
fun HomeScreen() {
    val viewModel: HomeViewModel = hiltViewModel()

    Content(
        viewState = viewModel.viewState.collectAsState(initial = ViewState.Loading).value
    )
}

@Preview
@Composable
private fun Content(viewState: ViewState = ViewState.Loading) {
    when (viewState) {
        ViewState.Loading -> {
            // Nothing
        }
        is ViewState.Loaded -> {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(16.dp, 24.dp),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.spacedBy(16.dp)
            ) {
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    text = stringResource(id = R.string.all_ad_will_be_blocked),
                    textAlign = TextAlign.Center,
                    style = MaterialTheme.typography.h3,
//                    color = MaterialTheme.colors.onPrimary//TODO: wrong way to do it?
                )
                Image(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(200.dp)
                        .padding(vertical = 16.dp),
                    alignment = Alignment.Center,
                    painter = painterResource(id = R.drawable.img_cat_2),
                    contentDescription = null
                )
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    text = stringResource(id = R.string.see_how_many_ad_was_already_defeated),
                    style = MaterialTheme.typography.h5
                )
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    text = "• Blocked Ads: ${viewState.blockedAdsCount}",
                )
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    text = "• Skipped Ads: ${viewState.skippedAdsCount}",
                )
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    text = "• Skipped surveys: ${viewState.skippedSurveysCount}",
                )
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    text = "• Blocked shorts: ${viewState.blockedShortsCount}",
                )
            }
        }
    }
}