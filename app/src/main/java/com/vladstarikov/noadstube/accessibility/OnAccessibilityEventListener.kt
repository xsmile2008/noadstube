package com.vladstarikov.noadstube.accessibility

import android.accessibilityservice.AccessibilityService
import android.view.accessibility.AccessibilityEvent

interface OnAccessibilityEventListener {

    fun onAccessibilityEvent(service: AccessibilityService, event: AccessibilityEvent)
}