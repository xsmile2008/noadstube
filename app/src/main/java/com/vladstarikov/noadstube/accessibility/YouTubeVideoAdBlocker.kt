package com.vladstarikov.noadstube.accessibility

import android.accessibilityservice.AccessibilityService
import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityNodeInfo.ACTION_CLICK
import com.vladstarikov.noadstube.R
import com.vladstarikov.noadstube.data.db.entities.LogRecord
import com.vladstarikov.noadstube.data.db.entities.LogRecord.Type
import com.vladstarikov.noadstube.data.repositories.LogsRepository
import com.vladstarikov.noadstube.data.repositories.PreferencesRepository
import com.vladstarikov.noadstube.utils.Toaster
import com.vladstarikov.noadstube.utils.YouTubeResources
import com.vladstarikov.noadstube.utils.extensions.checkAllIdsExists
import timber.log.Timber

//TODO: cover with tests
class YouTubeVideoAdBlocker(
    private val logsRepository: LogsRepository,
    private val preferencesRepository: PreferencesRepository,
    private val toaster: Toaster,
    private val youTubeResources: YouTubeResources
) : OnAccessibilityEventListener {

    private var timeAdBlockingStartedAt: Long? = null
    private val isAdBlockingStatedRecently: Boolean
        get() = timeAdBlockingStartedAt?.let {
            System.currentTimeMillis() - it < 15_000
        } ?: false

    override fun onAccessibilityEvent(service: AccessibilityService, event: AccessibilityEvent) {
        if (preferencesRepository.blockAd.value.not()) return
        val source = event.source ?: return

        if (!isAdBlockingStatedRecently) {
            /** ad_progress_text -> "Ad · 2 of 2 · 0:03" */
            source.findAccessibilityNodeInfosByViewId("com.google.android.youtube:id/ad_progress_text")
                .firstOrNull()
                ?.also {
                    /**
                     * Do nothing if it is survey
                     * TODO: experimental code
                     */
                    if (
                        source.findAccessibilityNodeInfosByText(
                            youTubeResources.getString(YouTubeResources.Strings.reel_accessibility_survey_page)
                        ).isNotEmpty()
                    ) {
                        Timber.w("YouTubeVideoAdBlocker will skip this event because `Survey` text was detected")
//                        Timber.d(source.buildAccessibilityTree())
                        return
                    }
                }
                ?.performAction(ACTION_CLICK)
                ?.let { success ->
                    if (success) {
                        Timber.d("Clicked on com.google.android.youtube:id/ad_progress_text")
                        timeAdBlockingStartedAt = System.currentTimeMillis()
                    }
                    return
                }
        } else {
            /**
             * null -> android.widget.FrameLayout | "null"
             *  com.google.android.youtube:id/coordinator -> android.view.ViewGroup | "null"
             *      com.google.android.youtube:id/design_bottom_sheet -> android.widget.FrameLayout | "null"
             *          null -> android.widget.ListView | "null"
             *              null -> android.view.ViewGroup | "null"
             *                  com.google.android.youtube:id/list_item_text -> android.widget.TextView | "Why this ad?"
             *              null -> android.view.ViewGroup | "null"
             *                  com.google.android.youtube:id/list_item_text -> android.widget.TextView | "Stop seeing this ad"
             *              null -> android.view.ViewGroup | "null"
             *                  com.google.android.youtube:id/list_item_text -> android.widget.TextView | "Control your Google ads"
             */
            if (source.checkAllIdsExists("com.google.android.youtube:id/design_bottom_sheet")) {
                Timber.d("Detected com.google.android.youtube:id/bottom_sheet_list_view")
                source.findAccessibilityNodeInfosByViewId("com.google.android.youtube:id/list_item_text")
                    .takeIf { it.size == 3 }
                    ?.get(1)
                    ?.let { stopSeeingThisAddNodeInfo ->
                        if (stopSeeingThisAddNodeInfo.parent.performAction(ACTION_CLICK)) {
                            Timber.d("Clicked on parent of $stopSeeingThisAddNodeInfo")
                        } else {
                            Timber.d("Failed to click on parent of $stopSeeingThisAddNodeInfo")
                        }
                        return
                    }
            }

            source.findAccessibilityNodeInfosByViewId("android:id/buttonPanel")
                .firstOrNull()
                ?.also { buttonPanel ->
                    Timber.d("Detected android:id/buttonPanel node")
                    when (buttonPanel.childCount) {
                        1 -> {
                            // Click "CLOSE" button on this dialog:

                            /**
                             *  null -> android.widget.FrameLayout | "null"
                             *  android:id/message -> android.widget.TextView | "This ad is based on:
                             *
                             * • The time of day or your general location (like your country or city)
                             *
                             * Report this ad
                             *
                             * Visit Google's Ad Settings to learn more about how ads are targeted or to opt out of personalized ads."
                             *  android:id/buttonPanel -> android.widget.ScrollView | "null"
                             *   android:id/button1 -> android.widgetk.Button | "CLOSE"
                             */
                            buttonPanel
                                .findAccessibilityNodeInfosByViewId("android:id/button1")
                                .firstOrNull()
                                ?.let { button1 ->
                                    if (button1.performAction(ACTION_CLICK)) {
                                        Timber.d("Clicked on $button1")
                                        if (preferencesRepository.showReports.value) {
                                            logsRepository.log(LogRecord(type = Type.AD_BLOCKED))
                                            toaster.showToast(
                                                text = R.string.message_ad_blocked,
                                                tag = "report",
                                            )
                                        }
                                    } else {
                                        Timber.d("Failed to click \"CLOSE\" button")
                                    }
                                }
                        }
                        2 -> {
                            if (source.checkAllIdsExists("android:id/alertTitle")) {
                                if (source.checkAllIdsExists("android:id/select_dialog_listview")) {
                                    // Click "CLOSE" button on this dialog:

                                    /**
                                     * android:id/alertTitle -> "It's gone. What was wrong with this ad?"
                                     * android:id/select_dialog_listview -> null
                                     * android:id/text1 -> "Irrelevant"
                                     * android:id/text1 -> "Inappropriate"
                                     * android:id/text1 -> "Repetitive"
                                     * android:id/buttonPanel -> null
                                     * android:id/button2 -> "CLOSE"
                                     * android:id/button1 -> "SEND"
                                     */
                                    buttonPanel
                                        .findAccessibilityNodeInfosByViewId("android:id/button2")
                                        .firstOrNull()
                                        ?.let { button2 ->
                                            if (button2.performAction(ACTION_CLICK)) {
                                                Timber.d("Clicked on $button2")
                                            } else {
                                                Timber.d("Failed to click on $button2")
                                            }
                                        }
                                } else {
                                    // Click "YES" button on this dialog:

                                    /**
                                     * android:id/alertTitle -> "Are you sure you want to stop seeing this ad?"
                                     * android:id/buttonPanel -> null
                                     * android:id/button2 -> "CANCEL"
                                     * android:id/button1 -> "YES"
                                     */
                                    buttonPanel
                                        .findAccessibilityNodeInfosByViewId("android:id/button1")
                                        .firstOrNull()
                                        ?.let { button1 ->
                                            if (button1.performAction(ACTION_CLICK)) {
                                                Timber.d("Clicked on $button1")
                                            } else {
                                                Timber.d("Failed to click on $button1")
                                            }
                                        }
                                }
                            }
                        }
                        else -> {
                            //Nothing
                        }
                    }
                }
        }
    }
}