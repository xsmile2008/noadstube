package com.vladstarikov.noadstube.accessibility

import android.accessibilityservice.AccessibilityService
import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityNodeInfo
import com.vladstarikov.noadstube.R
import com.vladstarikov.noadstube.data.db.entities.LogRecord
import com.vladstarikov.noadstube.data.db.entities.LogRecord.Type
import com.vladstarikov.noadstube.data.repositories.LogsRepository
import com.vladstarikov.noadstube.data.repositories.PreferencesRepository
import com.vladstarikov.noadstube.utils.Toaster
import com.vladstarikov.noadstube.utils.YouTubeResources
import com.vladstarikov.noadstube.utils.YouTubeResources.Strings.skip_survey
import timber.log.Timber

class YouTubeSurveySkipper(
    private val logsRepository: LogsRepository,
    private val preferencesRepository: PreferencesRepository,
    private val toaster: Toaster,
    private val youTubeResources: YouTubeResources
) : OnAccessibilityEventListener {

    override fun onAccessibilityEvent(service: AccessibilityService, event: AccessibilityEvent) {
        if (preferencesRepository.skipSurveys.value.not()) return
        val source = event.source ?: return

        source.findAccessibilityNodeInfosByText(youTubeResources.getString(skip_survey))
            .firstOrNull()
            ?.performAction(AccessibilityNodeInfo.ACTION_CLICK)
            ?.let { success: Boolean ->
                if (success) {
//                    Timber.i(source.buildAccessibilityTree())
                    Timber.d("Clicked on \"Skip survey\"")
                    if (preferencesRepository.showReports.value) {
                        logsRepository.log(LogRecord(type = Type.SURVEY_SKIPPED))
                        toaster.showToast(text = R.string.message_survey_skipped, tag = "report")
                    }
                } else {
                    Timber.w("Failed to click on \"Skip survey\"")
                }
            }
    }
}