package com.vladstarikov.noadstube.accessibility

import android.accessibilityservice.AccessibilityService
import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityNodeInfo
import com.vladstarikov.noadstube.R
import com.vladstarikov.noadstube.data.db.entities.LogRecord
import com.vladstarikov.noadstube.data.db.entities.LogRecord.Type
import com.vladstarikov.noadstube.data.repositories.LogsRepository
import com.vladstarikov.noadstube.data.repositories.PreferencesRepository
import com.vladstarikov.noadstube.utils.Toaster
import timber.log.Timber

class YouTubeVideoAdSkipper(
    private val logsRepository: LogsRepository,
    private val preferencesRepository: PreferencesRepository,
    private val toaster: Toaster
) : OnAccessibilityEventListener {

    override fun onAccessibilityEvent(service: AccessibilityService, event: AccessibilityEvent) {
        if (preferencesRepository.skipAd.value.not()) return
        val source = event.source ?: return

        // skip_ad_button -> "Skip Ad"
        source.findAccessibilityNodeInfosByViewId("com.google.android.youtube:id/skip_ad_button")
            .firstOrNull()
            ?.performAction(AccessibilityNodeInfo.ACTION_CLICK)
            ?.let { success: Boolean ->
                if (success) {
                    Timber.d("Clicked on skip_ad_button")
                    if (preferencesRepository.showReports.value) {
                        logsRepository.log(LogRecord(type = Type.AD_SKIPPED))
                        toaster.showToast(text = R.string.message_ad_skipped, tag = "report")
                    }
                } else {
                    Timber.w("Failed to click on skip_ad_button")
                }
            }
    }
}