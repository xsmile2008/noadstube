package com.vladstarikov.noadstube.accessibility

import android.accessibilityservice.AccessibilityService
import android.content.Context
import android.content.Intent
import android.view.accessibility.AccessibilityEvent
import com.vladstarikov.noadstube.ui.root.RootActivity
import com.vladstarikov.noadstube.utils.extensions.isAccessibilityEnabled
import timber.log.Timber

class AccessibilityManager(private val context: Context) : OnAccessibilityEventListener {

    // region Subscribers

    /** Order matters */
    private val subscribers: MutableList<OnAccessibilityEventListener> = mutableListOf()

    /** Subscribers will be called in order they was subscribed */
    fun subscribe(subscriber: OnAccessibilityEventListener) {
        if (subscribers.contains(subscriber).not()) {
            subscribers.add(subscriber)
        }
    }

    @Suppress("unused")
    fun unsubscribe(subscriber: OnAccessibilityEventListener) {
        subscribers.remove(subscriber)
    }
    // endregion

    val isAccessibilitySettingOn: Boolean
        get() = context.isAccessibilityEnabled(AppAccessibilityService::class.java)

    var allowAutoReturnToAppOnServiceConnection: Boolean = false

    fun onServiceConnected() {
        if (allowAutoReturnToAppOnServiceConnection) {
            allowAutoReturnToAppOnServiceConnection = false

            // Return from settings into our app
            context.startActivity(
                Intent(context, RootActivity::class.java).apply {
                    flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                }
            )
        }
    }

    //TODO: recycle events?
    //TODO: recycle source?
    override fun onAccessibilityEvent(service: AccessibilityService, event: AccessibilityEvent) {
        subscribers.forEach {
            try {
                it.onAccessibilityEvent(service, event)
            } catch (e: Exception) {
                // TODO: log to crashlytics
                Timber.e(e, "Error while calling subscriber: $it")
            }
        }
    }
}