package com.vladstarikov.noadstube.accessibility

import android.accessibilityservice.AccessibilityService
import android.view.accessibility.AccessibilityEvent
import com.vladstarikov.noadstube.data.db.entities.LogRecord
import com.vladstarikov.noadstube.data.repositories.LogsRepository
import com.vladstarikov.noadstube.data.repositories.PreferencesRepository
import com.vladstarikov.noadstube.utils.Toaster
import com.vladstarikov.noadstube.utils.extensions.treeFind
import timber.log.Timber
import java.util.Date
import kotlin.time.toTimeUnit

class YouTubeShortsBlocker(
    private val logsRepository: LogsRepository,
    private val preferencesRepository: PreferencesRepository,
    private val toaster: Toaster,
) : OnAccessibilityEventListener {

    /**
     * List of viewed shorts history.
     * key - date of viewing
     * value - title of viewed short
     */
    private val viewedShortsHistory = mutableListOf<Pair<Date, String>>()

    override fun onAccessibilityEvent(service: AccessibilityService, event: AccessibilityEvent) {
        val allowedShortsCount = preferencesRepository.allowedShortsCount.value ?: return
        val source = event.source ?: return

        val reelPlayerPageContainer = source.findAccessibilityNodeInfosByViewId(
            "com.google.android.youtube:id/reel_player_page_container",
        ).firstOrNull() ?: return

        val elementsTopChannelBarContainer = reelPlayerPageContainer.treeFind { node ->
            node.viewIdResourceName == "com.google.android.youtube:id/elements_top_channel_bar_container"
        } ?: return

        val channelTitle = elementsTopChannelBarContainer.treeFind { node ->
            node.className == "android.widget.Button" && node.text != null
        }?.text ?: return

        val videoTitle = reelPlayerPageContainer.treeFind { node ->
            node.viewIdResourceName == null && node.className == "android.view.ViewGroup" && node.text == null && node.childCount == 1
        }?.treeFind { node ->
            node.viewIdResourceName == null && node.text != null
        }?.text

        val composedName = "$channelTitle - $videoTitle"

        Timber.i("Detected YouTube Reel: $composedName")

        /// You can open short and watch it by opening it second time. Let it be not bug, but feature.
        if (viewedShortsHistory.lastOrNull()?.second != composedName) {
            if (allowedShortsCount.count == 0) {
                Timber.i("Blocking short: $composedName")
                service.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK)
                logsRepository.log(LogRecord(type = LogRecord.Type.SHORT_BLOCKED))
                toaster.showToast("You have blocked all shorts")
            } else {
                viewedShortsHistory.add(Date() to composedName)

                val countingDuration = allowedShortsCount.durationUnit.toTimeUnit().toMillis(1)
                val now = Date()
                val videosCount = viewedShortsHistory.count {
                    now.time - it.first.time < countingDuration
                }

                if (videosCount > allowedShortsCount.count) {
                    // Block this short
                    Timber.i("Blocking short: $composedName")
                    service.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK)
                    logsRepository.log(LogRecord(type = LogRecord.Type.SHORT_BLOCKED))
                    toaster.showToast(
                        "Your limit is ${allowedShortsCount.count} shorts per ${
                            allowedShortsCount.durationUnit.name.lowercase().dropLast(1)
                        }"
                    )
                }

                // Remove old viewed shorts from history
                viewedShortsHistory.removeAll { now.time - it.first.time > countingDuration }
            }
        }
    }
}