package com.vladstarikov.noadstube.di

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.PreferenceDataStoreFactory
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStoreFile
import com.vladstarikov.noadstube.accessibility.AccessibilityManager
import com.vladstarikov.noadstube.accessibility.YouTubeShortsBlocker
import com.vladstarikov.noadstube.accessibility.YouTubeSurveySkipper
import com.vladstarikov.noadstube.accessibility.YouTubeVideoAdBlocker
import com.vladstarikov.noadstube.accessibility.YouTubeVideoAdSkipper
import com.vladstarikov.noadstube.data.db.AppDatabase
import com.vladstarikov.noadstube.data.db.dao.LogRecordDao
import com.vladstarikov.noadstube.data.repositories.LogsRepository
import com.vladstarikov.noadstube.data.repositories.PreferencesRepository
import com.vladstarikov.noadstube.utils.Toaster
import com.vladstarikov.noadstube.utils.YouTubeResources
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class CommonModule {

    @Provides
    @Singleton
    fun providePreferencesDataStore(
        @ApplicationContext context: Context
    ): DataStore<Preferences> = PreferenceDataStoreFactory.create {
        context.preferencesDataStoreFile("settings")
    }

    @Provides
    @Singleton
    fun provideAppDatabase(
        @ApplicationContext context: Context
    ): AppDatabase = AppDatabase.build(context)

    @Provides
    @Singleton
    fun provideLogRecordDao(appDatabase: AppDatabase): LogRecordDao = appDatabase.logRecordDao()

    @Provides
    @Singleton
    fun provideAccessibilityManager(
        @ApplicationContext context: Context,
        logsRepository: LogsRepository,
        preferencesRepository: PreferencesRepository,
        toaster: Toaster,
        youTubeResources: YouTubeResources
    ) = AccessibilityManager(context).also {
        /** Subscribers will be called in order they was subscribed */

        it.subscribe(
            YouTubeVideoAdSkipper(
                logsRepository,
                preferencesRepository,
                toaster,
            )
        )
        it.subscribe(
            YouTubeVideoAdBlocker(
                logsRepository,
                preferencesRepository,
                toaster,
                youTubeResources,
            )
        )
        it.subscribe(
            YouTubeSurveySkipper(
                logsRepository,
                preferencesRepository,
                toaster,
                youTubeResources,
            )
        )
        it.subscribe(
            YouTubeShortsBlocker(
                logsRepository,
                preferencesRepository,
                toaster,
            )
        )
    }
}