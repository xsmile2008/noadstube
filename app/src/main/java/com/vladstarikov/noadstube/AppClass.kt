package com.vladstarikov.noadstube

import android.app.Application
import com.facebook.stetho.Stetho
import com.tonytangandroid.wood.WoodTree
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber

@HiltAndroidApp
class AppClass : Application() {

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
            Timber.plant(
                WoodTree(this)
                    .retainDataFor(WoodTree.Period.ONE_DAY)
                    .maxLength(100000).showNotification(true)
            )

            Stetho.initializeWithDefaults(this)
        } else {
            //Timber.plant(CrashReportingTree()) // TODO: implement this
        }
    }
}