package com.vladstarikov.noadstube.utils

import android.content.Context
import android.widget.Toast
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Toaster @Inject constructor(@ApplicationContext private val context: Context) {

    private val cachedToasts: MutableMap<String, Toast> = mutableMapOf()

    fun showToast(text: Int, duration: Int = Toast.LENGTH_SHORT, tag: String? = null) {
        showToast(context.getText(text), duration, tag)
    }

    fun showToast(text: CharSequence, duration: Int = Toast.LENGTH_SHORT, tag: String? = null) {
        (cachedToasts[tag] ?: Toast(context).also {
            if (tag != null) cachedToasts[tag] = it
        }).apply {
            setText(text)
            setDuration(duration)
            show()
        }
    }
}