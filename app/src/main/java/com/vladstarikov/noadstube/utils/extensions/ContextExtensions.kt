package com.vladstarikov.noadstube.utils.extensions

import android.accessibilityservice.AccessibilityService
import android.content.ComponentName
import android.content.Context
import android.provider.Settings
import timber.log.Timber

/**
 * [http://stackoverflow.com/questions/18094982/detect-if-my-accessibility-service-is-enabled](http://stackoverflow.com/questions/18094982/detect-if-my-accessibility-service-is-enabled)
 */
fun Context.isAccessibilityEnabled(clazz: Class<out AccessibilityService>): Boolean = try {
    if (Settings.Secure.getInt(contentResolver, Settings.Secure.ACCESSIBILITY_ENABLED) == 1) {
        val name = ComponentName(applicationContext, clazz)
        val services: String? = Settings.Secure.getString(
            contentResolver,
            Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES
        )
        services?.contains(name.flattenToString()) ?: false
    } else {
        false
    }
} catch (e: Settings.SettingNotFoundException) {
    Timber.e(e)
    false
}