package com.vladstarikov.noadstube.utils

import android.content.Context
import android.content.pm.PackageManager
import dagger.hilt.android.qualifiers.ApplicationContext
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class YouTubeResources @Inject constructor(
    @ApplicationContext private val context: Context
) {

    object Strings {

        /** "Close" */
        const val accessibility_close_button = "accessibility_close_button"

        /** "Cancel" */
        const val cancel = "cancel"

        /** "Yes" */
        const val delete_upload_dialog_positive = "delete_upload_dialog_positive"

        /** "Skip survey" */
        const val skip_survey = "skip_survey"

        /** "Submit" */
        const val inline_surveys_button_submit = "inline_surveys_button_submit"

        /** "Survey" */
        const val reel_accessibility_survey_page = "reel_accessibility_survey_page"
    }

    fun getString(key: String): String? = try {
        val res = context.packageManager.getResourcesForApplication("com.google.android.youtube")
        val resourceId: Int = res.getIdentifier(
            "com.google.android.youtube:string/$key",
            null,
            null
        )
        if (0 != resourceId) res.getString(resourceId) else null
    } catch (e: PackageManager.NameNotFoundException) {
        e.printStackTrace()
        null
    }
}