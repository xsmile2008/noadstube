@file:Suppress("unused")

package com.vladstarikov.noadstube.utils.extensions

import android.content.Context
import android.view.accessibility.AccessibilityNodeInfo
import androidx.annotation.StringRes
import timber.log.Timber

/** Returns a [MutableIterator] over the views in this view group. */
operator fun AccessibilityNodeInfo.iterator() = object : Iterator<AccessibilityNodeInfo> {
    private var index = 0
    override fun hasNext() = index < childCount
    override fun next(): AccessibilityNodeInfo =
        getChild(index++) ?: if (--index >= childCount) throw IndexOutOfBoundsException(
            "index: $index, childCount: $childCount"
        ) else throw NullPointerException("Child is null: index: $index, childCount: $childCount")
}

/** Returns a [Sequence] over the child views in this view group. */
val AccessibilityNodeInfo.children: Sequence<AccessibilityNodeInfo>
    get() = object : Sequence<AccessibilityNodeInfo> {
        override fun iterator() = this@children.iterator()
    }

fun AccessibilityNodeInfo.treeFind(
    predicate: (AccessibilityNodeInfo) -> Boolean
): AccessibilityNodeInfo? = if (predicate(this)) {
    this
} else {
    children.map { it.treeFind(predicate) }.firstOrNull { it != null }
}

// region Debug

/**
 * Use it in case you want to know other apps view ids to handle them via accessibility.
 * IMPORTANT: you must add flagReportViewIds flag to the accessibility service for this function to work
 */
@Suppress("unused")
fun AccessibilityNodeInfo.logViewIds() {
    Timber.d("Found view with id: $viewIdResourceName with text: ${text ?: contentDescription}")
    for (i in 0 until (childCount)) {
        getChild(i).logViewIds()
    }
}

/**
 * Only for debug
 */
@Suppress("unused")
fun AccessibilityNodeInfo?.buildAccessibilityTree(): String =
    buildAccessibilityTree(listOf(this))

/**
 * Only for debug
 */
@Suppress("unused")
private fun buildAccessibilityTree(
    nodes: List<AccessibilityNodeInfo?>,
    deep: Int = 0
): String {
    val stringBuilder = StringBuilder()
    nodes.filterNotNull().forEach { node ->
        stringBuilder.append('\n')
        for (i in 0 until deep) {
            stringBuilder.append('\t')
        }
        stringBuilder.append("${node.viewIdResourceName} -> ${node.className} | \"${node.text}\"")
        if (node.childCount > 0) {
            stringBuilder.append(buildAccessibilityTree(node.children.toList(), deep + 1))
        }
    }
    return stringBuilder.toString()
}
// endregion

// region Search by ID

fun AccessibilityNodeInfo.checkAllIdsExists(
    vararg ids: String
): Boolean = ids.all { findAccessibilityNodeInfosByViewId(it).isNotEmpty() }

fun AccessibilityNodeInfo.checkAnyIdsExists(
    vararg ids: String
): Boolean = ids.any { findAccessibilityNodeInfosByViewId(it).isNotEmpty() }

fun AccessibilityNodeInfo.checkNoneIdsExists(
    vararg ids: String
): Boolean = ids.none { findAccessibilityNodeInfosByViewId(it).isNotEmpty() }
// endregion

// region Search by text

fun AccessibilityNodeInfo.checkAllTextsExists(
    vararg texts: String
): Boolean = texts.all { findAccessibilityNodeInfosByText(it).isNotEmpty() }

fun AccessibilityNodeInfo.checkAnyTextsExists(
    vararg texts: String
): Boolean = texts.any { findAccessibilityNodeInfosByText(it).isNotEmpty() }

fun AccessibilityNodeInfo.checkNoneTextsExists(
    vararg texts: String
): Boolean = texts.none { findAccessibilityNodeInfosByText(it).isNotEmpty() }

fun AccessibilityNodeInfo.checkAllTextsExists(
    context: Context,
    @StringRes vararg texts: Int
): Boolean = texts.all {
    findAccessibilityNodeInfosByText(context.getString(it)).isNotEmpty()
}

fun AccessibilityNodeInfo.checkAnyTextsExists(
    context: Context,
    @StringRes vararg texts: Int
): Boolean = texts.any {
    findAccessibilityNodeInfosByText(context.getString(it)).isNotEmpty()
}

fun AccessibilityNodeInfo.checkNoneTextsExists(
    context: Context,
    @StringRes vararg texts: Int
): Boolean = texts.none {
    findAccessibilityNodeInfosByText(context.getString(it)).isNotEmpty()
}
// endregion
