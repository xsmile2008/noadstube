package com.vladstarikov.noadstube.accessibility

import android.accessibilityservice.AccessibilityService
import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityNodeInfo
import android.view.accessibility.AccessibilityNodeInfo.ACTION_CLICK
import com.vladstarikov.noadstube.R
import com.vladstarikov.noadstube.data.repositories.LogsRepository
import com.vladstarikov.noadstube.data.repositories.PreferencesRepository
import com.vladstarikov.noadstube.utils.Toaster
import com.vladstarikov.noadstube.utils.YouTubeResources
import com.vladstarikov.noadstube.utils.YouTubeResources.Strings.skip_survey
import io.mockk.called
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.flow.MutableStateFlow
import org.junit.Test

class YouTubeVideoSurveySkipperTest {

    // region Mocks

    private val accessibilityService: AccessibilityService = mockk(relaxed = true)
    private val logsRepository: LogsRepository = mockk(relaxed = true)
    private val preferencesRepository: PreferencesRepository = mockk(relaxed = true)
    private val toaster: Toaster = mockk(relaxed = true)
    private val youTubeResources: YouTubeResources = mockk {
        every { getString(skip_survey) } returns "Skip survey"
    }
    // endregion

    private val youTubeVideoAdSkipper = YouTubeSurveySkipper(
        logsRepository,
        preferencesRepository,
        toaster,
        youTubeResources,
    )

    @Test
    fun `Given skipSurveys is false, then should do nothing with event`() {
        //Given
        every { preferencesRepository.skipSurveys } returns MutableStateFlow(false)

        //When
        val event: AccessibilityEvent = mockk(relaxed = true)
        youTubeVideoAdSkipper.onAccessibilityEvent(accessibilityService, event)

        //Then
        verify {
            event wasNot called
        }
    }

    @Test
    fun `Given skipSurveys is true and event contains node with Skip survey text, when onAccessibilityEvent was called with this event, then should perform ACTION_CLICK on Skip survey button`() {
        //Given
        every { preferencesRepository.skipSurveys } returns MutableStateFlow(true)

        val skipSurveysButtonNodeInfo: AccessibilityNodeInfo = mockk(relaxed = true)
        val event: AccessibilityEvent = mockk {
            every { source } returns mockk {
                every {
                    findAccessibilityNodeInfosByText("Skip survey")
                } returns listOf(skipSurveysButtonNodeInfo)
            }
        }

        //When
        youTubeVideoAdSkipper.onAccessibilityEvent(accessibilityService, event)

        //Then
        verify {
            skipSurveysButtonNodeInfo.performAction(ACTION_CLICK)
        }
    }

    @Test
    fun `Given showReportToasts is true, when ACTION_CLICK was performed successfully on Skip survey button, then should show message_survey_skipped toast`() {
        //Given
        every { preferencesRepository.skipSurveys } returns MutableStateFlow(true) // Required to pass
        every { preferencesRepository.showReports } returns MutableStateFlow(true)

        //When
        youTubeVideoAdSkipper.onAccessibilityEvent(
            accessibilityService,
            mockk {
                every { source } returns mockk {
                    every {
                        findAccessibilityNodeInfosByText("Skip survey")
                    } returns listOf(
                        mockk(relaxed = true) {
                            every { performAction(ACTION_CLICK) } returns true
                        }
                    )
                }
            }
        )

        //Then
        verify {
            toaster.showToast(text = R.string.message_survey_skipped, tag = "report")
        }
    }

    @Test
    fun `Given showReportToasts is false, when ACTION_CLICK was performed successfully on Skip survey button, then should not show any toast`() {
        //Given
        every { preferencesRepository.skipSurveys } returns MutableStateFlow(true) // Required to pass
        every { preferencesRepository.showReports } returns MutableStateFlow(false)

        //When
        youTubeVideoAdSkipper.onAccessibilityEvent(
            accessibilityService,
            mockk {
                every { source } returns mockk {
                    every {
                        findAccessibilityNodeInfosByText("Skip survey")
                    } returns listOf(
                        mockk(relaxed = true) {
                            every { performAction(ACTION_CLICK) } returns true
                        }
                    )
                }
            }
        )

        //Then
        verify {
            toaster wasNot called
        }
    }
}