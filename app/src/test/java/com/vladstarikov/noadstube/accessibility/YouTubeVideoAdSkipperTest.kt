package com.vladstarikov.noadstube.accessibility

import android.accessibilityservice.AccessibilityService
import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityNodeInfo
import android.view.accessibility.AccessibilityNodeInfo.ACTION_CLICK
import com.vladstarikov.noadstube.R
import com.vladstarikov.noadstube.data.repositories.LogsRepository
import com.vladstarikov.noadstube.data.repositories.PreferencesRepository
import com.vladstarikov.noadstube.utils.Toaster
import io.mockk.*
import kotlinx.coroutines.flow.MutableStateFlow
import org.junit.Test

class YouTubeVideoAdSkipperTest {

    // region Mocks

    private val accessibilityService: AccessibilityService = mockk(relaxed = true)
    private val logsRepository: LogsRepository = mockk(relaxed = true)
    private val preferencesRepository: PreferencesRepository = mockk(relaxed = true)
    private val toaster: Toaster = mockk(relaxed = true)
    // endregion

    private val youTubeVideoAdSkipper = YouTubeVideoAdSkipper(
        logsRepository,
        preferencesRepository,
        toaster,
    )

    @Test
    fun `Given skipAd is false, then should do nothing with event`() {
        //Given
        every { preferencesRepository.skipAd } returns MutableStateFlow(false)

        //When
        val event: AccessibilityEvent = mockk(relaxed = true)
        youTubeVideoAdSkipper.onAccessibilityEvent(accessibilityService, event)

        //Then
        verify {
            event wasNot called
        }
    }

    @Test
    fun `Given skipAd is true and event contains node with skip_ad_button ID, when onAccessibilityEvent was called with this event, then should perform ACTION_CLICK on skip_ad_button node`() {
        //Given
        every { preferencesRepository.skipAd } returns MutableStateFlow(true)

        val skipAdButtonNodeInfo: AccessibilityNodeInfo = mockk(relaxed = true)
        val event: AccessibilityEvent = mockk {
            every { source } returns mockk {
                every {
                    findAccessibilityNodeInfosByViewId("com.google.android.youtube:id/skip_ad_button")
                } returns listOf(skipAdButtonNodeInfo)
            }
        }

        //When
        youTubeVideoAdSkipper.onAccessibilityEvent(accessibilityService, event)

        //Then
        verify {
            skipAdButtonNodeInfo.performAction(ACTION_CLICK)
        }
    }

    @Test
    fun `Given showReportToasts is true, when ACTION_CLICK was performed successfully on skip_ad_button, then should show message_ad_skipped toast`() {
        //Given
        every { preferencesRepository.skipAd } returns MutableStateFlow(true) // Required to pass
        every { preferencesRepository.showReports } returns MutableStateFlow(true)

        //When
        youTubeVideoAdSkipper.onAccessibilityEvent(accessibilityService, mockk {
            every { source } returns mockk {
                every {
                    findAccessibilityNodeInfosByViewId("com.google.android.youtube:id/skip_ad_button")
                } returns listOf(mockk(relaxed = true) {
                    every { performAction(ACTION_CLICK) } returns true
                })
            }
        })

        //Then
        verify {
            toaster.showToast(text = R.string.message_ad_skipped, tag = "report")
        }
    }

    @Test
    fun `Given showReportToasts is false, when ACTION_CLICK was performed successfully on skip_ad_button, then should not show any toast`() {
        //Given
        every { preferencesRepository.skipAd } returns MutableStateFlow(true) // Required to pass
        every { preferencesRepository.showReports } returns MutableStateFlow(false)

        //When
        youTubeVideoAdSkipper.onAccessibilityEvent(accessibilityService, mockk {
            every { source } returns mockk {
                every {
                    findAccessibilityNodeInfosByViewId("com.google.android.youtube:id/skip_ad_button")
                } returns listOf(mockk(relaxed = true) {
                    every { performAction(ACTION_CLICK) } returns true
                })
            }
        })

        //Then
        verify {
            toaster wasNot called
        }
    }
}