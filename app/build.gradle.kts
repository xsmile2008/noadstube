plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("kapt")
    id("com.google.gms.google-services")
    id("com.google.firebase.crashlytics")
    id("dagger.hilt.android.plugin")
}

android {
    compileSdk = Config.Android.compileSdk

    defaultConfig {
        applicationId = Config.Android.applicationId
        minSdk = Config.Android.minSdk
        targetSdk = Config.Android.targetSdk
        versionCode = Config.Android.versionCode
        versionName = Config.Android.versionName

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("debug") {
            isMinifyEnabled = false
        }
        getByName("release") {
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }
    kotlinOptions {
        jvmTarget = "11"
    }
    kapt {
        arguments {
            arg("room.schemaLocation", "$projectDir/schemas")
            arg("room.incremental", "true")
        }
    }
    buildFeatures {
        compose = true
        buildConfig = true
    }

    composeOptions {
        kotlinCompilerExtensionVersion = Config.Versions.composeCompiler
    }

    namespace = "com.vladstarikov.noadstube"
}

dependencies {
    // Kotlin
    implementation(kotlin("stdlib-jdk8:${Config.Versions.kotlin}"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:${Config.Versions.coroutines}")

    // AndroidX
    implementation("androidx.core:core-ktx:1.12.0")
    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation("androidx.navigation:navigation-compose:2.7.5")
    implementation("androidx.datastore:datastore-preferences:1.0.0")

    // Lifecycle
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:${Config.Versions.lifecycle}")
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:${Config.Versions.lifecycle}")
    implementation("androidx.lifecycle:lifecycle-livedata-ktx:${Config.Versions.lifecycle}")

    // Compose
    implementation("androidx.compose.ui:ui:${Config.Versions.compose}")
    //// Tooling support (Previews, etc.)
    implementation("androidx.compose.ui:ui-tooling:${Config.Versions.compose}")
    //// Foundation (Border, Background, Box, Image, Scroll, shapes, animations, etc.)
    implementation("androidx.compose.foundation:foundation:${Config.Versions.compose}")
    //// Material Design
    implementation("androidx.compose.material:material:${Config.Versions.compose}")
    //// Integration with observables
    implementation("androidx.compose.runtime:runtime-livedata:${Config.Versions.compose}")

    // Room
    implementation("androidx.room:room-runtime:${Config.Versions.room}")
    implementation("androidx.room:room-ktx:${Config.Versions.room}")
    kapt("androidx.room:room-compiler:${Config.Versions.room}")
    testImplementation("androidx.room:room-testing:${Config.Versions.room}")

    // Google
    implementation("com.google.android.material:material:1.10.0")

    // Firebase
    implementation(platform("com.google.firebase:firebase-bom:30.5.0"))
    implementation("com.google.firebase:firebase-crashlytics-ktx")
    implementation("com.google.firebase:firebase-analytics-ktx")

    // Timber
    implementation("com.jakewharton.timber:timber:5.0.1")
    debugImplementation("com.github.TonyTangAndroid.Wood:wood:0.9.9.2")
    releaseImplementation("com.github.TonyTangAndroid.Wood:wood-no-op:0.9.9.2")

    // Stetho
    implementation("com.facebook.stetho:stetho:1.6.0")

    // Tests
    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")
    testImplementation("io.mockk:mockk:${Config.Versions.mockk}")

    // DI Hilt
    implementation("com.google.dagger:hilt-android:${Config.Versions.hilt}")
    kapt("com.google.dagger:hilt-android-compiler:${Config.Versions.hilt}")
    implementation("androidx.hilt:hilt-navigation-compose:1.1.0")
}

// Allow references to generated code (Needed for Hilt)
kapt {
    correctErrorTypes = true
}
