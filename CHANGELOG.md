# Changelog

## 2021-09-23 (6.10.8)
- Detect and close dialog in YouTubeAdBlocker using view IDs instead of YouTube stings
- Update libs
- Remove some deprecated APIs usage
