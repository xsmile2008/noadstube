object Config {
    object Android {
        const val applicationId = "com.vladstarikov.noadstube"
        const val compileSdk = 34
        const val minSdk = 26
        const val targetSdk = 34
        const val versionCode = 9
        const val versionName = "0.7"
    }

    object Versions {
        const val kotlin = "1.9.20"
        const val coroutines = "1.7.3"
        const val compose = "1.5.4"
        const val composeCompiler = "1.5.5"
        const val lifecycle = "2.6.2"
        const val mockk = "1.13.8"
        const val hilt = "2.48"
        const val room = "2.6.1"
    }
}